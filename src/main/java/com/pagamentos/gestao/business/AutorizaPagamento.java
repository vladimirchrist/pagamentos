package com.pagamentos.gestao.business;

import com.pagamentos.gestao.business.chain.DiretorFinanceiroHandler;
import com.pagamentos.gestao.business.chain.DiretorGeralHandler;
import com.pagamentos.gestao.business.chain.GerenteGeralHandler;
import com.pagamentos.gestao.business.chain.GerenteImediatoHandler;
import com.pagamentos.gestao.business.chain.PagamentoChain;
import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;

public class AutorizaPagamento
{
    private Pagamento pagamento;
    private Usuario usuario;
    
    public AutorizaPagamento(Pagamento pagamento, Usuario usuario)
    {
        this.pagamento = pagamento;
        this.usuario = usuario;
    }

    public void autorizar() throws Exception
    {
        PagamentoChain p = new PagamentoChain();
        p.addPagamentoHandler(new GerenteImediatoHandler());
        p.addPagamentoHandler(new GerenteGeralHandler());
        p.addPagamentoHandler(new DiretorFinanceiroHandler());
        p.addPagamentoHandler(new DiretorGeralHandler());

        p.handleRequest(pagamento, usuario);
    }

}

package com.pagamentos.gestao.business.chain;

import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;

public abstract class AbstractPagamentoHander implements PagamentoHandler
{
    private String usuario;
    private PagamentoHandler next;

    @Override
    public void setNext(PagamentoHandler next)
    {
        this.next = next;
    }

    @Override
    public void handleRequest(Pagamento pagamento, Usuario usuario) throws Exception
    {
        if (accept(pagamento, usuario))
        {
            doHandle(pagamento, usuario);
        } else if (this.next != null)
        {
            this.next.handleRequest(pagamento, usuario);
        } else
        {
            throw new Exception("Não foi possível aprovar o pagamento.");
        }  
    }    

    public abstract boolean accept(Pagamento pagamento, Usuario usuario);

    public abstract void doHandle(Pagamento pagamento, Usuario usuario);
    
}

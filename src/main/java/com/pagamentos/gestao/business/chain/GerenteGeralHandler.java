package com.pagamentos.gestao.business.chain;

import com.pagamentos.gestao.model.Ocorrencia;
import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;
import java.time.Instant;
import java.util.Date;

public class GerenteGeralHandler extends AbstractPagamentoHander
{

    private final double limiteAprovacao = 1500;
    private boolean disponivel = true;

    @Override
    public boolean accept(Pagamento pagamento, Usuario usuario)
    {
        boolean accept = false;
        if (limiteAprovacao >= pagamento.getValor()
                && !pagamento.getStatus().equals("vencido")
                && this.disponivel)
        {
            accept = true;
          } else {
            pagamento.addOcorrencia(new Ocorrencia(0, Date.from(Instant.now()),
                    "O Gerente Geral não pôde realizar o pagamento no valor de "+pagamento.getValor()+
                            " por ser superior ao seu limite " + this.limiteAprovacao, usuario.getNome()));
        }      
        
        return accept;
    }
    
   @Override
    public void doHandle(Pagamento pagamento, Usuario usuario)
    {
                pagamento.setStatus("autorizado");
        pagamento.addOcorrencia(new Ocorrencia(0, Date.from(Instant.now()),
                "Pagamento no valor de " + pagamento.getValor()
                + " aprovado pelo Gerente Geral", usuario.getNome()));

    }
    
    public void setDisponivel(boolean disponivel)
    {
        this.disponivel = disponivel;
    }


    
}

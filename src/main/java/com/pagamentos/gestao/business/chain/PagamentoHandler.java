package com.pagamentos.gestao.business.chain;

import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;

public interface PagamentoHandler
{
    public void setNext(PagamentoHandler handler);
    public void handleRequest(Pagamento pagamento, Usuario usuario) throws Exception; 
}

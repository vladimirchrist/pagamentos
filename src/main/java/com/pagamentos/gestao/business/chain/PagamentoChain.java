package com.pagamentos.gestao.business.chain;

import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;

public class PagamentoChain
{
    private PagamentoHandler first;
    private PagamentoHandler sucessor;

    public void addPagamentoHandler(PagamentoHandler pagamentoHandler)
    {

        if (this.first == null)
        {
            this.first = pagamentoHandler;
        } else
        {
            this.sucessor.setNext(pagamentoHandler);
        }

        this.sucessor = pagamentoHandler;
    }

    public void handleRequest(Pagamento pagamento, Usuario usuario) throws Exception
    {
        first.handleRequest(pagamento, usuario);
    }

}

package com.pagamentos.gestao.business.chain;

import com.pagamentos.gestao.model.Ocorrencia;
import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;
import java.time.Instant;
import java.util.Date;

public class GerenteImediatoHandler extends AbstractPagamentoHander
{

    private boolean disponivel = true;
    private final double limiteAprovacao = 500;

    @Override
    public boolean accept(Pagamento pagamento, Usuario usuario)
    {
        boolean accept = false;
        if (limiteAprovacao >= pagamento.getValor()
                && !pagamento.getStatus().equals("vencido")
                && this.disponivel)
        {
            accept = true;
        } else
        {
            pagamento.setStatus("recusado");
            pagamento.addOcorrencia(new Ocorrencia(0, Date.from(Instant.now()),
                    "O Gerente Imediato não pôde realizar o pagamento no valor de " + pagamento.getValor()
                    + " por ser superior ao seu limite " + this.limiteAprovacao, usuario.getNome()));
        }

        return accept;
    }

    @Override
    public void doHandle(Pagamento pagamento, Usuario usuario)
    {
        pagamento.setStatus("autorizado");
        pagamento.addOcorrencia(new Ocorrencia(0, Date.from(Instant.now()),
                "Pagamento no valor de " + pagamento.getValor()
                + " aprovado pelo Gerente Imediato", usuario.getNome()));

    }

    public void setDisponivel(boolean disponivel)
    {
        this.disponivel = disponivel;
    }

}

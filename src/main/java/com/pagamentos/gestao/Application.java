package com.pagamentos.gestao;

import com.pagamentos.gestao.model.Funcionario;
import com.pagamentos.gestao.model.Ocorrencia;
import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;
import com.pagamentos.gestao.repository.UserRepository;
import com.pagamentos.gestao.service.IFuncionarioService;
import com.pagamentos.gestao.service.IOcorrenciaService;
import com.pagamentos.gestao.service.IPagamentoService;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application
{

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner runner(IPagamentoService pService, 
            IFuncionarioService fService, 
            IOcorrenciaService oService,
            UserRepository uRepsitory)
    {
        return args ->
        {           
            fService.save(new Funcionario(1L, "João", "Gerente"));
            fService.save(new Funcionario(2L, "Maria", "Gerente Geral"));
            Funcionario f = new Funcionario(3L, "Adalberto", "Diretor Imediato");
            fService.save(f);
            Pagamento p = new Pagamento(1L, f, "Reserva semanal Hotel", 254.56, new SimpleDateFormat("dd/MM/yyyy").parse("12/05/2020"));
            pService.save(p);
        };
    }
}

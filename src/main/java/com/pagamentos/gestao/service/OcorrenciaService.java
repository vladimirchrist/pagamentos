package com.pagamentos.gestao.service;

import com.pagamentos.gestao.model.Ocorrencia;
import com.pagamentos.gestao.repository.OcorrenciaRepository;
import org.springframework.stereotype.Service;

@Service
public class OcorrenciaService implements IOcorrenciaService{
    private final OcorrenciaRepository ocorrenciaRepository;

    
    public OcorrenciaService(OcorrenciaRepository ocorrenciaRepository)
    {
        this.ocorrenciaRepository = ocorrenciaRepository;
    }
    
    @Override
    public Ocorrencia save(Ocorrencia ocorrencia){
        return ocorrenciaRepository.save(ocorrencia);
    }
    
    //public Iterable<Ocorrencia> getAllOcorrencias(){
        
    //}
}
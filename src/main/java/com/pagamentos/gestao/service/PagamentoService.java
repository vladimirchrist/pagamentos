package com.pagamentos.gestao.service;

import com.pagamentos.gestao.business.AutorizaPagamento;
import com.pagamentos.gestao.exception.ResourceNotFoundException;
import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.model.Usuario;
import com.pagamentos.gestao.repository.PagamentoRepository;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService implements IPagamentoService{
    
    private final PagamentoRepository pagamentoRepository;

    public PagamentoService(PagamentoRepository pagamentoRepository)
    {
        this.pagamentoRepository = pagamentoRepository;
    }
    
    @Override
    public Pagamento save (Pagamento pagamento) {        
        return pagamentoRepository.save(pagamento);
    }
    
    @Override
    public Pagamento findById(Long id) {
        return pagamentoRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Pagamento não encontrado!"));
    }
    
    @Override
    public Iterable<Pagamento> getAllPagamentos(){
        return pagamentoRepository.findAll();
    }
    
    @Override
    public Pagamento autorizarPagamento(Pagamento pagamento) throws Exception {

        AutorizaPagamento autoriza = new AutorizaPagamento(pagamento, 
            new Usuario(1l, "Vladimir"));

        autoriza.autorizar();
        this.save(pagamento);
        return pagamento;
    }

}
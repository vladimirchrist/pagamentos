
package com.pagamentos.gestao.service;

import com.pagamentos.gestao.model.Ocorrencia;

public interface IOcorrenciaService
{

    Ocorrencia save(Ocorrencia ocorrencia);
    
}

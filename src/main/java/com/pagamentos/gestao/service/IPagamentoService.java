package com.pagamentos.gestao.service;

import com.pagamentos.gestao.model.Pagamento;

public interface IPagamentoService
{

    public Pagamento findById(Long id);
    public Iterable<Pagamento> getAllPagamentos();
    public Pagamento save(Pagamento pagamento);
    public Pagamento autorizarPagamento(Pagamento pagamento) throws Exception;
}

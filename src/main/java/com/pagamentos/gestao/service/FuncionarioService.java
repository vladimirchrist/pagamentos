package com.pagamentos.gestao.service;

import com.pagamentos.gestao.exception.ResourceNotFoundException;
import com.pagamentos.gestao.model.Funcionario;
import com.pagamentos.gestao.repository.FuncionarioRepository;
import org.springframework.stereotype.Service;

@Service
public class FuncionarioService implements IFuncionarioService {

    private final FuncionarioRepository funcionarioRepository;

    public FuncionarioService(FuncionarioRepository funcionarioRepository) {
        this.funcionarioRepository = funcionarioRepository;
    }

    @Override
    public Funcionario save(Funcionario funcionario) {
        return funcionarioRepository.save(funcionario);
    }

    @Override
    public Funcionario findById(Long id) throws ResourceNotFoundException {
        return funcionarioRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Funcionario não encontrado"));
    }

    @Override
    public Iterable<Funcionario> getAllFuncionarios() {
        return funcionarioRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {
        funcionarioRepository.deleteById(id);
    }

    @Override
    public void delete(Funcionario funcionario) {
        funcionarioRepository.delete(funcionario);
    }

}
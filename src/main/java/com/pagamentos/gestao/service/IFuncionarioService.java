package com.pagamentos.gestao.service;

import com.pagamentos.gestao.model.Funcionario;

public interface IFuncionarioService
{

    public void delete(Funcionario funcionario);
    public void deleteById(Long id);
    public Funcionario findById(Long id);
    public Iterable<Funcionario> getAllFuncionarios();
    public Funcionario save(Funcionario funcionario);
    
}

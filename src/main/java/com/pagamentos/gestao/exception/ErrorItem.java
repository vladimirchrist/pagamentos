package com.pagamentos.gestao.exception;

import com.fasterxml.jackson.annotation.JsonInclude;

class ErrorItem
{

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String code;

    private String message;

    void setCode(String code)
    {
        this.code = code;
    }

    void setMessage(String message)
    {
        this.message = message;
    }

    public String getCode()
    {
        return code;
    }

    public String getMessage()
    {
        return message;
    }
    
    

}

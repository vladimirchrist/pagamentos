package com.pagamentos.gestao.repository;

import com.pagamentos.gestao.model.Ocorrencia;
import org.springframework.data.repository.CrudRepository;

public interface OcorrenciaRepository extends CrudRepository<Ocorrencia, Long> {}
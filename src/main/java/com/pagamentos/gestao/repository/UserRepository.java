package com.pagamentos.gestao.repository;

import com.pagamentos.gestao.model.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Usuario, Long> {}
package com.pagamentos.gestao.repository;

import com.pagamentos.gestao.model.Funcionario;
import org.springframework.data.repository.CrudRepository;

public interface FuncionarioRepository extends CrudRepository<Funcionario, Long> {}
package com.pagamentos.gestao.repository;

import com.pagamentos.gestao.model.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {}
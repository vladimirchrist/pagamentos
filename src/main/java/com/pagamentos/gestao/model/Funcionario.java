package com.pagamentos.gestao.model;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Funcionario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idFuncionario;

    @NotNull(message = "Nome do funcionário é obrigatório.")
    @Basic(optional = false)
    private String nome;

    @NotNull(message = "Cargo do funcionario é obrigatório.")
    @Basic(optional = false)
    private String cargo;

    public Funcionario(long id, String nome, String cargo) {
        this.idFuncionario = id;
        this.nome = nome;
        this.cargo = cargo;
    }

    public Funcionario() { }

    public Long getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(Long idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    

}
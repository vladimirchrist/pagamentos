package com.pagamentos.gestao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Pagamento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPagamento;

    @ManyToOne
    @JoinColumn(name = "idFuncionario", referencedColumnName = "idFuncionario")
    private Funcionario solicitante;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "idPagamento", referencedColumnName = "idPagamento")
    private List<Ocorrencia> ocorrencias;

    private String descricao;
    private String status;
    private double valor;
    private Date dataVencimento;
    private Date dataPagamento;

    public Pagamento() {
    }

    public Pagamento(long id, Funcionario solicitante, String descricao, double valor, Date dataVencimento)
            throws Exception {
        this.status = "solicitado";
        this.idPagamento = id;
        this.ocorrencias = new ArrayList<>();
        this.solicitante = solicitante;
        this.descricao = descricao;
        this.valor = valor;
        this.dataVencimento = dataVencimento;
    }

    public void addOcorrencia(Ocorrencia ocorrencia) {
        this.ocorrencias.add(ocorrencia);
    }

    public Long getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(Long idPagamento) {
        this.idPagamento = idPagamento;
    }

    public Funcionario getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(Funcionario solicitante) {
        this.solicitante = solicitante;
    }

    public List<Ocorrencia> getOcorrencias() {
        return ocorrencias;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        System.out.println(dataVencimento);
            this.dataVencimento =dataVencimento;

    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

}

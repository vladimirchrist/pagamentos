package com.pagamentos.gestao.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Ocorrencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idOcorrencia;
    
    private Date data;
    private String descricao;
    private String usuario;

    public Ocorrencia() { }

    public Ocorrencia(long idOcorrencia, Date data, String descricao, String usuario)
    {
        this.idOcorrencia = idOcorrencia;
        this.data = data;
        this.descricao = descricao;
        this.usuario = usuario;
    }
       
    public Long getId()
    {
        return idOcorrencia;
    }

    public void setId(Long id)
    {
        this.idOcorrencia = id;
    }

    public Date getData()
    {
        return data;
    }

    public void setData(Date data)
    {
        this.data = data;
    }

    public String getDescricao()
    {
        return descricao;
    }

    public void setDescricao(String descricao)
    {
        this.descricao = descricao;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }
    
}

package com.pagamentos.gestao.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Usuario
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUsuario;
    
    private String nome;
    
    public Usuario() {}

    public Usuario(long id, String nome)
    {
        this.idUsuario = id;
        this.nome = nome;
    }
   
    public Long getIdUsuario()
    {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario)
    {
        this.idUsuario = idUsuario;
    }

    public String getNome()
    {
        return nome;
    }

    public void setNome(String nome)
    {
        this.nome = nome;
    }
    
    
    
}

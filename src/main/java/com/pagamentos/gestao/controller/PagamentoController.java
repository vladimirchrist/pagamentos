package com.pagamentos.gestao.controller;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.pagamentos.gestao.model.Pagamento;
import com.pagamentos.gestao.service.IPagamentoService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/pagamentos")
public class PagamentoController
{

    private final IPagamentoService pagamentoService;

    public PagamentoController(IPagamentoService pagamentoService)
    {
        this.pagamentoService = pagamentoService;
    }

    @PostMapping
    public void addPagamento(@RequestBody Pagamento pagamento)
    {
        this.pagamentoService.save(pagamento);
    }

    @PostMapping("/autorizar")
    public void autorizarPagamento(@RequestBody long[] id) throws Exception
    {

        ArrayList<Pagamento> pagamentos = new ArrayList<>();

        for (long l : id)
        {

            System.out.println("\n\n" + l);

            Pagamento p = this.pagamentoService.findById(l);
            pagamentos.add(p);
        }

        for (Pagamento p : pagamentos)
        {
            this.pagamentoService.autorizarPagamento(p);
        }
    }

    @GetMapping
    public List<Pagamento> getPagamentos()
    {
        return (List<Pagamento>) this.pagamentoService.getAllPagamentos();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pagamento> getById(@PathVariable(value = "id") long idPagamento)
    {
        Pagamento p = this.pagamentoService.findById(idPagamento);
        p.toString();
        return ResponseEntity.ok().body(p);
    }

    @GetMapping("/naoautorizado")
    public List<Pagamento> getPagamentosNaoAutorizados()
    {
        List<Pagamento> pagamentos = (List<Pagamento>) this.pagamentoService.getAllPagamentos();
        List<Pagamento> naoAutorizados = new ArrayList<>();
        for (Pagamento pagamento : pagamentos)
        {
            if (!pagamento.getStatus().equals("pago")
                    && !pagamento.getStatus().equals("autorizado")
                    && pagamento.getDataVencimento().toInstant()
                            .atZone(ZoneId.systemDefault()).toLocalDate().isAfter(LocalDate.now()))
            {
                naoAutorizados.add(pagamento);
            }
        }
        return naoAutorizados;
    }

}

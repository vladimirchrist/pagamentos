package com.pagamentos.gestao.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.pagamentos.gestao.model.Funcionario;
import com.pagamentos.gestao.service.IFuncionarioService;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/funcionarios")
public class FuncionarioController
{
    private final IFuncionarioService funcionarioService;

    public FuncionarioController(IFuncionarioService funcionarioService)
    {
        this.funcionarioService = funcionarioService;
    }
    
    @PostMapping
    public void addFuncionario (@RequestBody Funcionario funcionario){
        funcionarioService.save(funcionario);
    }

    @GetMapping
    public List<Funcionario> getFuncionarios() {
        return (List<Funcionario>)this.funcionarioService.getAllFuncionarios();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Funcionario> getFuncionarioById(@PathVariable(value = "id") long idFuncionario){
        Funcionario f = funcionarioService.findById(idFuncionario);        
        return ResponseEntity.ok().body(f);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Funcionario> atualizarFuncionario(@PathVariable(value = "id") long idFuncionario,
            @Valid @RequestBody Funcionario funcionario){
        
        Funcionario f = funcionarioService.findById(idFuncionario);
        f.setNome(funcionario.getNome());
        f.setCargo(funcionario.getCargo());
        
        final Funcionario funcionarioAtualizado = funcionarioService.save(f);
        return ResponseEntity.ok(funcionarioAtualizado);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteFuncionario(@PathVariable(value = "id") long idFuncionario){

        Funcionario funcionario = this.funcionarioService.findById(idFuncionario);
        this.funcionarioService.delete(funcionario);

        Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;    
    }
}

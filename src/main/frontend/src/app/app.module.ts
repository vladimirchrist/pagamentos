import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FuncionarioFormComponent } from './funcionarios/funcionario-form/funcionario-form.component';
import { FuncionarioService } from './funcionarios/service/funcionario.service';
import { PagamentoFormComponent } from './pagamentos/pagamento-form/pagamento-form.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FuncionarioListComponent } from './funcionarios/funcionarios-list/funcionarios-list.component';
import { FuncionarioUpdateComponent } from './funcionarios/funcionario-update/funcionario-update.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { ClickOutsideDirective } from 'ng-click-outside';
import { SearchFilterPipe } from './model/filter-pipe';
import { MatFormFieldModule } from '@angular/material/form-field';
import { PagamentosListComponent } from './pagamentos/pagamentos-list/pagamentos-list.component';
import { AutorizarPagamentosComponent } from './pagamentos/autorizar-pagamentos/autorizar-pagamentos.component';
import { OcorrenciasListComponent } from './ocorrencias/ocorrencias-list/ocorrencias-list.component';
import { AutorizadoresListComponent } from './autorizadores/autorizadores-list/autorizadores-list.component';

@NgModule({
  declarations: [
    AppComponent,
    FuncionarioFormComponent,
    PagamentoFormComponent,
    FuncionarioListComponent,
    AutorizarPagamentosComponent,
    PagamentosListComponent,
    FuncionarioUpdateComponent,
    ClickOutsideDirective,
    SearchFilterPipe,
    OcorrenciasListComponent,
    AutorizadoresListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    Ng2SearchPipeModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule
  ],
  providers: [FuncionarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }

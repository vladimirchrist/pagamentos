import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  private collapsed = true;

  public toggleCollapsed() {
    this.collapsed = !this.collapsed;
  }

  public getCollapsed(): boolean {
    return this.collapsed;
  }
}

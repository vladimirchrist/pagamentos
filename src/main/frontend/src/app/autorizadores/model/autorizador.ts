export class Autorizador {
    idAutorizador: string;
    nome: string;
    cargo: string;     
    limiteAutorizacao: number;
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizadoresListComponent } from './autorizadores-list.component';

describe('AutorizadoresListComponent', () => {
  let component: AutorizadoresListComponent;
  let fixture: ComponentFixture<AutorizadoresListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizadoresListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizadoresListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

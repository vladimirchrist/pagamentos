import { Component, OnInit } from '@angular/core';
import { Autorizador } from '../model/autorizador';

@Component({
  selector: 'app-autorizadores-list',
  templateUrl: './autorizadores-list.component.html',
  styleUrls: ['./autorizadores-list.component.css']
})
export class AutorizadoresListComponent implements OnInit {

  autorizadores : Autorizador[] = [];

  constructor() { }

  ngOnInit() {
  }
  
  onChange(autorizador: Autorizador, isChecked: boolean) {
    if (isChecked) {
      this.autorizadores.push(autorizador);
    } else {
      let index = this.autorizadores.indexOf(autorizador);
      this.autorizadores.splice(index, 1);
    }
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'searchFuncionario' })
export class SearchFilterPipe implements PipeTransform {
    transform(itens: any, search: string): any {
        if (!search) { return itens; }
        let solution = itens.filter(funcionario => {
            if (!funcionario) { return; }
            return funcionario.nome.toLowerCase().indexOf(search.toLowerCase()) !== -1;
        })
        return solution;
    }
}
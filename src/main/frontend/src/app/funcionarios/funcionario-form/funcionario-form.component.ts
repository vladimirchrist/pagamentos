import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Funcionario } from '../model/funcionario';
import { FuncionarioService } from '../service/funcionario.service';


@Component({
  selector: 'funcionario-form',
  templateUrl: './funcionario-form.component.html',
  styleUrls: ['./funcionario-form.component.css']
})
export class FuncionarioFormComponent {
  funcionario: Funcionario;

  constructor(private funcionarioService: FuncionarioService,
    private router: Router) {
    this.funcionario = new Funcionario();
  }

  onSubmit() {
    this.funcionarioService.save(this.funcionario)
      .subscribe(result => {
        alert("Funcionário cadastrado!");
      });
  }
}

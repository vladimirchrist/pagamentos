import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Funcionario } from '../model/funcionario';

@Injectable()
export class FuncionarioService {

  private funcionariosUrl = "/api/funcionarios";

  constructor(private http: HttpClient) { }
  
  public save(funcionario: Funcionario) {
    return this.http.post<Funcionario>(this.funcionariosUrl, funcionario);
  }

  public deleteFuncionario(id: string): Observable<any> {
    return this.http.delete(`${this.funcionariosUrl}/${id}`, { responseType: 'text' });
  }

  public findById(id: number): Observable<any> {
    return this.http.get(`${this.funcionariosUrl}/${id}`)
  }

  public findAll(): Observable<Funcionario[]>{
    return this.http.get<Funcionario[]>(this.funcionariosUrl);
  }
  
  public updateFuncionario(id: number, funcionario: any) : Observable<Object>{
    return this.http.put(`${this.funcionariosUrl}/${id}`, funcionario);
  }
}

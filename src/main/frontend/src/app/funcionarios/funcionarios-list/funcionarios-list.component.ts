import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../model/funcionario';
import { FuncionarioService } from '../service/funcionario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'funcionarios-list',
  templateUrl: './funcionarios-list.component.html',
  styleUrls: ['./funcionarios-list.component.css']
})
export class FuncionarioListComponent implements OnInit {
  
  searchText: any;
  nomeFuncionario: String;
  funcionarios: Funcionario[];

  constructor(private funcionarioService: FuncionarioService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  deleteFuncionario(id: string) {
    this.funcionarioService.deleteFuncionario(id)
      .subscribe(data => {
          alert("Funcionario excluido!");
          console.log(data);
          this.reloadData();
        },
        (error) => console.log(error));
  }

  updateFuncionario(id: string) {
    this.router.navigate(['update', id]);
  }

  reloadData() {
    this.funcionarioService.findAll()
      .subscribe(data => {
        this.funcionarios = data;
      });
  }

}

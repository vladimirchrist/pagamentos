import { Component, OnInit } from '@angular/core';
import { Funcionario } from '../model/funcionario';
import { FuncionarioService } from '../service/funcionario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-funcionario-update',
  templateUrl: './funcionario-update.component.html',
  styleUrls: ['./funcionario-update.component.css']
})
export class FuncionarioUpdateComponent implements OnInit {

  id: number;
  funcionario: Funcionario;

  constructor( private route : ActivatedRoute,
    private router : Router,
    private funcionarioService: FuncionarioService) { }

  ngOnInit() {
    this.funcionario = new Funcionario();

    this.id = this.route.snapshot.params['id'];

    this.funcionarioService.findById(this.id)
      .subscribe(data => {
        this.funcionario = data;        
      }, error => console.log(error));
  }

  atualizarFuncionario() {
    this.funcionarioService.updateFuncionario(this.id, this.funcionario)
      .subscribe(data => {
        alert("Cadastro atualizado!");
        console.log(data) 
      }, error => console.log(error));
    this.funcionario = new Funcionario();
    this.gotoList();
  }

  gotoList() {
    this.router.navigate(['funcionarios']);
  }

  onSubmit() {
    this.atualizarFuncionario();
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FuncionarioFormComponent } from './funcionarios/funcionario-form/funcionario-form.component';
import { PagamentoFormComponent } from './pagamentos/pagamento-form/pagamento-form.component';
import { FuncionarioListComponent } from './funcionarios/funcionarios-list/funcionarios-list.component';
import { AutorizarPagamentosComponent } from './pagamentos/autorizar-pagamentos/autorizar-pagamentos.component';
import { FuncionarioUpdateComponent } from './funcionarios/funcionario-update/funcionario-update.component';
import { PagamentosListComponent } from './pagamentos/pagamentos-list/pagamentos-list.component';
import { OcorrenciasListComponent } from './ocorrencias/ocorrencias-list/ocorrencias-list.component';
import { AutorizadoresListComponent } from './autorizadores/autorizadores-list/autorizadores-list.component';

const routes: Routes = [
  {
    path: 'funcionarios',
    component: FuncionarioListComponent
  },
  {
    path: 'addFuncionario',
    component: FuncionarioFormComponent
  },
  {
    path: 'addPagamento',
    component: PagamentoFormComponent
  },
  {
    path: 'autorizarPagamento',
    component: AutorizarPagamentosComponent
  },
  {
    path: 'listarPagamento',
    component: PagamentosListComponent
  },
  {
    path: 'listarOcorrencias/:id',
    component: OcorrenciasListComponent
  },
  {
    path: 'update/:id',
    component: FuncionarioUpdateComponent
  },
  {
    path: 'autorizadores',
    component: AutorizadoresListComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

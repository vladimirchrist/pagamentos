import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pagamento } from '../model/pagamento';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PagamentoService {

  id: Number[];
  private pagamentosUrl = "/api/pagamentos";

  constructor(private http: HttpClient) { }

  public findById(id: number): Observable<any> {
    return this.http.get<Pagamento>(`${this.pagamentosUrl}/${id}`);
  }

  public save(pagamento: Pagamento) {
    return this.http.post<Pagamento>(this.pagamentosUrl, pagamento);
  }

  public getPagamentos(): Observable<Pagamento[]> {
    return this.http.get<Pagamento[]>(this.pagamentosUrl);
  }

  public getPagamentosNaoAutorizados(): Observable<Pagamento[]> {
    return this.http.get<Pagamento[]>(`${this.pagamentosUrl}/naoautorizado`);
  }

  public autorizarPagamento(pagamentos: Pagamento[]) {

    this.id = [];
    pagamentos.forEach(pagamento => {
      this.id.push(pagamento.idPagamento);
    });

    console.log(this.id);
    return this.http.post<Number[]>(`${this.pagamentosUrl}/autorizar`, this.id);
  }

}

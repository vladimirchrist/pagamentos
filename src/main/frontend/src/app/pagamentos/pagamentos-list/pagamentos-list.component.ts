import { Component, OnInit } from '@angular/core';
import { PagamentoService } from '../service/pagamento-service.service';
import { Pagamento } from '../model/pagamento';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagamentos-list',
  templateUrl: './pagamentos-list.component.html',
  styleUrls: ['./pagamentos-list.component.css']
})
export class PagamentosListComponent implements OnInit {

  pagamentos: Pagamento[] = [];
  pagamentosFilter: Pagamento[] = [];
  statusPagamento: String[];

  public summaries: any[];


  constructor(private router: Router,
    private pagamentoService: PagamentoService) {

  }

  ngOnInit() {
    this.reloadData();
    this.filter(0);
  }

  reloadData() {
    this.pagamentoService.getPagamentos()
      .subscribe(data => {
        this.pagamentos = data;
        this.statusPagamento = [];
        this.pagamentos.forEach(pagamento => {

          if (!this.statusPagamento.includes(pagamento.status))
            this.statusPagamento.push(pagamento.status);

        });
        this.pagamentosFilter = data;
      });
  }

  public listarOcorrencias(id: string): void {
    this.router.navigate(['listarOcorrencias', id]);
  }

  filter(status: any) {

    this.pagamentosFilter = [];

    if (status !== "0") {
      this.pagamentos.forEach(pagamento => {
        if (pagamento.status === status)
          this.pagamentosFilter.push(pagamento);
      });
    } else {
      this.pagamentosFilter = this.pagamentos;
    }
    
  }
}

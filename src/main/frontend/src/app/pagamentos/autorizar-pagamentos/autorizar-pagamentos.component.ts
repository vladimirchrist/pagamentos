import { Component, OnInit } from '@angular/core';
import { Pagamento } from '../model/pagamento';
import { PagamentoService } from '../service/pagamento-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-autorizar-pagamentos.',
  templateUrl: './autorizar-pagamentos.component.html',
  styleUrls: ['./autorizar-pagamentos.component.css']
})

export class AutorizarPagamentosComponent implements OnInit {

  searchText : any;
  pagamentosNaoAutorizados : Pagamento[];
  pagamentosAutorizados: Pagamento[] = [];
  dataAtual = Date.now;

  constructor(private pagamentoService: PagamentoService,
    private router: Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.pagamentoService.getPagamentosNaoAutorizados()
      .subscribe(data => {
        this.pagamentosNaoAutorizados = data;
      })
  }

  onChange(pagamento: Pagamento, isChecked: boolean) {
    if (isChecked) {
      this.pagamentosAutorizados.push(pagamento);
    } else {
      let index = this.pagamentosAutorizados.indexOf(pagamento);
      this.pagamentosAutorizados.splice(index, 1);
    }
  }
  
  public autorizar(pagamentos: Pagamento[]){
    this.pagamentoService.autorizarPagamento(pagamentos)
      .subscribe(result => {
        alert("Autorizados");
        this.reloadData();
      });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutorizarPagamentosComponent } from './autorizar-pagamentos.component';

describe('AutorizarPagamentosComponent', () => {
  let component: AutorizarPagamentosComponent;
  let fixture: ComponentFixture<AutorizarPagamentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutorizarPagamentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutorizarPagamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

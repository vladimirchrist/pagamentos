import { Component, OnInit } from '@angular/core';
import { Pagamento } from '../model/pagamento';
import { FuncionarioService } from 'src/app/funcionarios/service/funcionario.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PagamentoService } from '../service/pagamento-service.service';
import { Funcionario } from 'src/app/funcionarios/model/funcionario';

@Component({
  selector: 'pagamento-form',
  templateUrl: './pagamento-form.component.html',
  styleUrls: ['./pagamento-form.component.css']
})

export class PagamentoFormComponent implements OnInit {

  pagamentoForm : FormGroup;
  private showing = false;

  pagamento: Pagamento;
  funcionarios: Funcionario[];

  constructor(private funcionarioService: FuncionarioService,
    private pagamentoService: PagamentoService,
    private fb: FormBuilder) {
      this.pagamento = new Pagamento;
      this.initForm()
  }

  initForm(): FormGroup {
    return this.pagamentoForm = this.fb.group({
      solicitante: [null],
      descricao: new FormControl(''),
      valor: new FormControl(''),
      vencimento: new FormControl('')
    })
  }

  ngOnInit() {
    this.funcionarioService.findAll()
      .subscribe(data => {
        this.funcionarios = data;
      })
  }

  onSubmit() {
    this.pagamento.descricao = this.pagamentoForm.value.descricao;
    this.pagamento.valor = this.pagamentoForm.value.valor;
    this.pagamento.dataVencimento = new Date(this.pagamentoForm.value.vencimento);
    this.pagamento.status = "solicitado";
    this.pagamentoService.save(this.pagamento)
      .subscribe(result => {
        alert("Pagamento cadastrado!")
      });
  }

  toggleDropDown() {
    this.showing = !this.showing;
  }

  closeDropDown(){
    this.showing = false;
  }

  getShowing() {
    return this.showing;
  }

  getSearchValue() {
    return this.pagamentoForm.value.solicitantePagamento;
  }

  selectValue(funcionario: Funcionario) {
    this.pagamentoForm.patchValue({"solicitante": funcionario.nome});
    this.pagamento.solicitante = funcionario;
    this.showing = false;
  }

}

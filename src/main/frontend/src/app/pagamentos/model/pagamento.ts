import { Ocorrencia } from '../../ocorrencias/ocorrencia';
import { Funcionario } from '../../funcionarios/model/funcionario';

export class Pagamento {
    idPagamento : number;
    status: string;
    solicitante: Funcionario;
    ocorrencias: Ocorrencia[];
    descricao: string;
    valor: number;
    dataVencimento: Date;
    dataPagamento: Date;

}
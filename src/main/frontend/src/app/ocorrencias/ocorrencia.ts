export class Ocorrencia {
    id: string;
    data: Date;
    descricao: string;
    usuario: string;
}
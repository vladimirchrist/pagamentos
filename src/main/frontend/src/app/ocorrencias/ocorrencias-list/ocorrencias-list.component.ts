import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PagamentoService } from 'src/app/pagamentos/service/pagamento-service.service';
import { Pagamento } from 'src/app/pagamentos/model/pagamento';
import { Ocorrencia } from '../ocorrencia';

@Component({
  selector: 'app-ocorrencias-list',
  templateUrl: './ocorrencias-list.component.html',
  styleUrls: ['./ocorrencias-list.component.css']
})
export class OcorrenciasListComponent implements OnInit {

  id: number;
  pagamento: Pagamento;
  ocorrencias: Ocorrencia[];

  constructor(private route: ActivatedRoute,
    private pagamentoService: PagamentoService) { }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];

    this.pagamentoService.findById(this.id)
      .subscribe(data => {
        this.pagamento = data;
        this.ocorrencias = this.pagamento.ocorrencias;
      });
         
  }

}
